package com.example.woramatej.tlayer;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    private AudioManager audioManager;
    private SeekBar volumeControler;
    private boolean showingControls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        final View view = getLayoutInflater().inflate(R.layout.activity_main, null );
        volumeControler = (SeekBar) view.findViewById( R.id.volume_seekbar );
        volumeControler.setMax( audioManager.getStreamMaxVolume( AudioManager.STREAM_MUSIC ) );
//        volumeControler.setOnSeekBarChangeListener( ( SeekBar.OnSeekBarChangeListener) this );

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
//        ActionBar actionBar = getActionBar();
//        actionBar.hide();
    }
}
